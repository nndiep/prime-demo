FROM node:14.15-alpine AS ANGULAR_BUILD
WORKDIR /ui
COPY /ui/package.json /ui/package-lock.json ./
RUN npm install
COPY /ui .
RUN npm run build

FROM golang:1.15.4-alpine AS GO_BUILD
WORKDIR /server
COPY . .
# remove ui folder
RUN rm -Rf /ui
RUN go build ./server.go

FROM alpine:3.12.1
WORKDIR /app
COPY --from=ANGULAR_BUILD /ui/dist/ui/* ./ui/dist/ui/
COPY --from=GO_BUILD ./server/server ./
ENV IS_INCLUDE_UI=true
CMD ./server
