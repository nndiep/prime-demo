# Some notes may be useful for easy understand how the application is implemented

## Implement back-end project

````
go mod init gitlab.com/nndiep/prime-demo
go get github.com/99designs/gqlgen
go run github.com/99designs/gqlgen init
````

* Define the schema by updating `graph/schema.graphqls`
* Update `gqlgen.yml`: use <code>Int model</code> value: <code>github.com/99designs/gqlgen/graphql.Int64</code>
* Run this command to auto generate code
````
go run github.com/99designs/gqlgen generate
````
* Implement functions in `schema.resolvers.go`
- Implement `prime.go`, `prime_test.go` in `internal/prime` folder, then functions in `schema.resolvers.go` can use them.

## Implement front-end project
````
ng new ui
cd ui
ng add @angular/material
npm install @angular/flex-layout @angular/cdk --save
npm install --save-dev @graphql-codegen/cli
npx graphql-codegen init
npm install
````
Refer: https://graphql-code-generator.com/docs/getting-started/installation

* Start the backend, go to http://localhost:8080/, Click DOWNLOAD and choose SDL, copy the `schema.graphql` to `src/app/schema.graphql`

* Write graphql in `src/app/prime.graphql`
* Run this command to auto generate code
````
npm run graphql-codegen
````
* Implement the UI, discover the source code for more details

## Build docker image
````
docker build -t prime-demo .
````