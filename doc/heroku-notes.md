````
heroku login
docker tag prime-demo web
heroku container:login
heroku container:push web -a prime-demo-web
heroku container:release web -a prime-demo-web
heroku ps:scale web=1 -a prime-demo-web
````

View the app at https://prime-demo-web.herokuapp.com/home