module gitlab.com/nndiep/prime-demo

go 1.15

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/vektah/gqlparser/v2 v2.1.0
)
