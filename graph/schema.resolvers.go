package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/nndiep/prime-demo/graph/generated"
	"gitlab.com/nndiep/prime-demo/graph/model"
	"gitlab.com/nndiep/prime-demo/internal/prime"
)

func (r *mutationResolver) GetHighestPrimeNumber(ctx context.Context, number int64) (int64, error) {
	return prime.GetHighestPrimeNumber(number)
}

func (r *queryResolver) InputNumberHistories(ctx context.Context) ([]*model.InputNumberHistory, error) {
	return prime.InputNumberHistories()
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
