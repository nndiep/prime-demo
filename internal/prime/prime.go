package prime

import (
	"errors"
	"fmt"

	"gitlab.com/nndiep/prime-demo/graph/model"
)

// Get highest prime number lower than the input number
func GetHighestPrimeNumber(number int64) (int64, error) {
	if number < 0 {
		return 0, errors.New("Only accept to input positve number")
	}

	if number < 3 {
		return 0, fmt.Errorf("There's no prime number lower than %v", number)
	}

	startNumber := takeStartNumber(number)

	var i int64

	for i = startNumber; i >= 2; {
		isPrime := checkPrime(i)

		if isPrime == true {
			return i, nil
		}

		i -= 2
	}

	return i, nil
}

// Take the start number as an odd number except 2
func takeStartNumber(number int64) int64 {
	startNumber := number - 1
	if startNumber != 2 && startNumber%2 == 0 {
		startNumber -= 1
	}
	return startNumber
}

func checkPrime(n int64) bool {
	if n <= 3 {
		return true
	}

	if n%2 == 0 || n%3 == 0 {
		return false
	}
	var i int64
	for i = 5; i*2 <= n; {
		if n%i == 0 || n%(i+2) == 0 {
			return false
		}
		i += 6
	}
	return true
}

func InputNumberHistories() ([]*model.InputNumberHistory, error) {
	panic(fmt.Errorf("not implemented"))
}
