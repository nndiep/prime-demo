package prime

import (
	"testing"
)

func TestGetHighestPrimeNumber(t *testing.T) {
	t.Run("Error incase input negative number", func(t *testing.T) {

		invalidNumbers := []int64{-1, -10, -100, -9007199254740991}
		for _, number := range invalidNumbers {
			res, err := GetHighestPrimeNumber(number)

			if err == nil {
				t.Errorf("Expected error for input %v but received %v", number, res)
			}

			expected := "Only accept to input positve number"

			if err.Error() != expected {
				t.Errorf("Unexpected error for input %v. Expected \"%v\", got \"%v\"", number, expected, err.Error())
			}
		}
	})

	t.Run("Should return error: There's no prime number lower than the input number", func(t *testing.T) {

		invalidTests := []struct {
			number   int64
			expected string
		}{
			{0, "There's no prime number lower than 0"},
			{1, "There's no prime number lower than 1"},
			{2, "There's no prime number lower than 2"},
		}
		for _, invalidTest := range invalidTests {
			res, err := GetHighestPrimeNumber(invalidTest.number)

			if err == nil {
				t.Errorf("Expected error for input %v but received %v", invalidTest.number, res)
			}

			if err.Error() != invalidTest.expected {
				t.Errorf("Unexpected error for input %v. Expected \"%v\", got \"%v\"", invalidTest.number, invalidTest.expected, err.Error())
			}
		}
	})

	t.Run("Should return highest prime number lower than the input number", func(t *testing.T) {
		validTests := []struct {
			number   int64
			expected int64
		}{
			{3, 2},
			{4, 3},
			{5, 3},
			{6, 5},
			{55, 53},
			{12, 11},
			{13, 11},
			{14, 13},
			{15, 13},
			{16, 13},
			{17, 13},
			{18, 17},
			{19, 17},
			{20, 19},
			// {9007199254740991, 9007199254740991}, // update the algorithm to improve the performance for this test
		}
		for _, validTest := range validTests {
			res, err := GetHighestPrimeNumber(validTest.number)

			if err != nil {
				t.Errorf("Unexpected error for input %v but received %v", validTest.number, err.Error())
			}

			if res != validTest.expected {
				t.Errorf("Unexpected result for input %v. Expected \"%v\", got \"%v\"", validTest.number, validTest.expected, res)
			}
		}
	})
}
