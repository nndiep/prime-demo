import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { GetHighestPrimeNumberGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loading = false;
  error: string | null;
  maxNumber = Number.MAX_SAFE_INTEGER;

  highestPrimeNumber: number;

  form: FormGroup = new FormGroup({
    number: new FormControl('', [Validators.required, Validators.min(0), Validators.max(this.maxNumber)]),
  });

  constructor(
    private getHighestPrimeNumber: GetHighestPrimeNumberGQL,
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    delete this.highestPrimeNumber;
    delete this.error;

    if (this.form.valid) {
      this.loading = true;

      const number = this.number.value;

      this.getHighestPrimeNumber.mutate({ number }).
        pipe(finalize(() => this.loading = false)).
        subscribe(
          res => {
            this.highestPrimeNumber = res.data.getHighestPrimeNumber;
          }, err => {
            if (err.message) {
              this.error = err.message;
            } else {
              this.error = 'Uh oh. Something went wrong!'
            }
          });
    }
  }

  get number() {
    return this.form.get('number');
  }
}
