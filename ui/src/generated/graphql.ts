import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};



export type InputNumberHistory = {
  __typename?: 'InputNumberHistory';
  number: Scalars['Int'];
  highestPrimeNumber: Scalars['Int'];
};

export type Mutation = {
  __typename?: 'Mutation';
  getHighestPrimeNumber: Scalars['Int'];
};


export type MutationGetHighestPrimeNumberArgs = {
  number: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  inputNumberHistories?: Maybe<Array<InputNumberHistory>>;
};

export type GetHighestPrimeNumberMutationVariables = Exact<{
  number: Scalars['Int'];
}>;


export type GetHighestPrimeNumberMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'getHighestPrimeNumber'>
);

export const GetHighestPrimeNumberDocument = gql`
    mutation getHighestPrimeNumber($number: Int!) {
  getHighestPrimeNumber(number: $number)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetHighestPrimeNumberGQL extends Apollo.Mutation<GetHighestPrimeNumberMutation, GetHighestPrimeNumberMutationVariables> {
    document = GetHighestPrimeNumberDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }